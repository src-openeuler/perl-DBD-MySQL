Name:           perl-DBD-MySQL
Version:        5.011
Release:        1
Summary:        Perl [DBI] driver for access to MySQL databases.
License:        GPL-1.0-or-later or Artistic-1.0
URL:            https://metacpan.org/release/DBD-mysql
Source0:        https://cpan.metacpan.org/authors/id/D/DV/DVEEDEN/DBD-mysql-%{version}.tar.gz

BuildRequires: coreutils findutils gcc
BuildRequires: pkgconfig(mysqlclient)
BuildRequires: perl-generators perl-devel
BuildRequires: perl(ExtUtils::MakeMaker)
# configure
BuildRequires: perl(DBI) >= 1.609
BuildRequires: perl(Data::Dumper)
BuildRequires: perl(Devel::CheckLib) >= 1.09
# test
BuildRequires: perl(Test::Deep)
BuildRequires: perl(Test::Simple) >= 0.90
BuildRequires: perl(Time::HiRes)
BuildRequires: perl(bigint)
Provides:      perl-DBD-mysql = %{version}-%{release}

%{?perl_default_filter}

%description
DBD::mysql is the Perl5 Database Interface driver for the MySQL database. In other words: DBD::mysql is an
interface between the Perl programming language and the MySQL programming API that comes with the MySQL
relational database management system. Most functions provided by this programming API are supported. Some
rarely used functions are missing, mainly because no-one ever requested them.

%package help
Summary: Including man files for perl-DBD-MySQL
Buildarch: noarch

%description   help
This contains man files for the using of perl-DBD-MySQL.

%prep
%autosetup -n DBD-mysql-%{version} -p1
find . -type f ! -name \*.pl -print0 | xargs -0 chmod 644

%build
perl Makefile.PL INSTALLDIRS=vendor OPTIMIZE="%{optflags}" NO_PACKLIST=1
%make_build

%install
make pure_install DESTDIR=%{buildroot}
%{_fixperms} %{buildroot}/*

%files
%license LICENSE META.*
%doc Changes README.md
%{perl_vendorarch}/*

%files help
%{_mandir}/man3/*.3*

%changelog
* Tue Jan 07 2025 Funda Wang <fundawang@yeah.net> - 5.011-1
- update to 5.011
- WARNING: only mysql server >= 8.0 can be used now.
  For mariadb server, please use perl-DBD-MariaDB.

* Thu Nov 17 2022 dillon chen <dillon.chen@gmail.com> - 4.050-4
- Patch0 remove a useless shebang

* Sat Jun 25 2022 Xinyi Gou <plerks@163.com> - 4.050-3
- Specify license version

* Fri Jul 30 2021 chenyanpanHW <chenyanpan@huawei.com> - 4.050-2
- DESC: delete -Sgit from %autosetup, and delete BuildRequires git

* Thu Jan 14 2021 yanglongkang <yanglongkang@huawei.com> - 4.050-1
- update version 4.050

* Mon Aug 10 2020 volcanodragon <linfeilong@huawei.com> - 4.064-7
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:update yaml file

* Tue Oct 15 2019 shenyangyang<shenyangyang4@huawei.com> - 4.064-6
- Type:enhancemnet
- ID:NA
- SUG:NA
- DESC:delete unneeded build requires

* Sat Aug 31 2019 zoujing<zoujing13@huawei.com> - 4.064-5
- Type:enhancemnet
- ID:NA
- SUG:restart
- DESC:openEuler Debranding

* Wed Aug 21 2019 renxudong<renxudong1@huawei.com> - 4.064-4.1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:revise patch name

* Wed Jul 18 2018 openEuler Buildteam <buildteam@openeuler.org> - 4.046-4
- Package init
